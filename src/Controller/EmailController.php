<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class EmailController extends AbstractController
{
    /**
     * @Route("/email")
     */
    public function sentEmail(MailerInterface $mailer){

        $email = (new Email())
        ->from('asda@dsd.pl')
        ->to('text@text.pl')
        ->subject("sdads")
        ->html('<b>Siema</b>')
        ->attachFromPath('/Users/mariuszsliwowski/ww.txt')
        ;

        $mailer->send($email);

        return new Response('Sent Email');
    }

}